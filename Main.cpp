#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Algorithms/LinearProgramming.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Policies/EpsilonPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Utils/LP.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <AIToolbox/Factored/MDP/Algorithms/SparseCooperativeQLearning.hpp>
#include <algorithm>

#include <math.h>       /* pow */
#include <string>
#include "Utils.cpp"
#include "Simulation.cpp"
#include "SCQL.cpp"
#include "Train.cpp"
#include "Community.cpp"
#include "Exchange.cpp"
#include <iostream>

int main (int argc, char *args[]) {

	// ./Main simple $groupsize$ $networksize$ $FLOW$
	if(std::string(args[1]) == "simple"){
		if (argc < 6){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		// size 1,4,9,16,32
		int groupsize = atoi(args[2]);
		// should always be to power of 2
		int networksize = atoi(args[3]);
		//Amount of cars being spawned/ time step
		int flow = atoi(args[4]);
		//how many trainings until next group
		int ttrain = atoi(args[5]);
		//set location of hotspot in middle
		AVG=atoi(args[6]);
		//set std of distribution
		STD= atoi(args[7]);

		std::cout<< "Network of "<<networksize << std::endl;
		std::cout<< "Agents in groups of "<< groupsize*groupsize << std::endl;
		std::cout<< "Flow through network "<< flow << std::endl;

		string str1 = "Networks size: "+to_string(networksize)+ "\n";
		string str2 = "Group size: "+ to_string(groupsize * groupsize) +"\n";
		string str3 = "Flow: "+ to_string(flow) +"\n";

		//Create Filename
		string temp = "results/simple_"+ to_string(networksize) +"_"+ to_string(groupsize * groupsize) +"_" + to_string(flow)+"_" + to_string(ttrain)+ ".txt";
		FILENAME = temp.c_str();
		createFile( str1+ str2+str3);

		//Trains & Creates Q greedy policies for each group
		std::vector<std::vector<int>> groups = inertialPartitioning( networksize, groupsize,  groupsize);
		trainSpecific(groups,networksize, flow, false, ttrain);
	}

if(std::string(args[1]) == "multioverlap"){
		if (argc < 6){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		// size 1,4,9,16,32
		int groupsize = atoi(args[2]);
		int networksize = atoi(args[3]);
		int flow = atoi(args[4]);//Amount of cars being spawned/ time step
		int ttrain = atoi(args[5]);//how many trainings until next group
		AVG=atoi(args[6]);	//set location of hotspot in middle
		STD= atoi(args[7]);//set std of distribution

		int overlap = atoi(args[8]);
		//Create overlapping groups
		std::vector<std::vector<int>> groups = inertialPartitioning( networksize, groupsize,  groupsize, overlap );
		findMultiples(groups, networksize);
		printGroup(groups);
		groupToNetwork(groups, networksize);

		//Train overlaping groups.
		trainSpecific(groups, networksize,flow, false, ttrain);
}



	if(std::string(args[1]) == "overlap"){
		if (argc < 6){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		// size 1,4,9,16,32
		int groupsize = atoi(args[2]);
		int networksize = atoi(args[3]);
		int flow = atoi(args[4]);//Amount of cars being spawned/ time step
		int ttrain = atoi(args[5]);//how many trainings until next group
		AVG=atoi(args[6]);	//set location of hotspot in middle
		STD= atoi(args[7]);//set std of distribution

		//Create overlapping groups
		std::vector<std::vector<int>> groups = overlapPartitioning( networksize, groupsize,  groupsize );
		printGroup(groups);
		groupToNetwork(groups, networksize);

		//Train overlaping groups.
		trainSpecific(groups,networksize, flow, false, ttrain);
	}

	if(std::string(args[1]) == "overlapborder"){
		if (argc < 6){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		// size 1,4,9,16,32
		int groupsize = atoi(args[2]);
		int networksize = atoi(args[3]);
		int flow = atoi(args[4]);//Amount of cars being spawned/ time step
		int ttrain = atoi(args[5]);//how many trainings until next group
		AVG=atoi(args[6]);	//set location of hotspot in middle
		STD= atoi(args[7]);//set std of distribution

		//Create overlapping groups
		std::vector<std::vector<int>> groups = overlapborderPartitioning( networksize, groupsize,  groupsize );
		printGroup(groups);
		groupToNetwork(groups, networksize);

		//Train overlaping groups.
		trainSpecific(groups, networksize,flow, false, ttrain);
	}

//bottom up merge
//start with 1 agent groups.
//merge groups with problem.
	if(std::string(args[1]) == "merge"){
		if (argc < 4){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		// size 1,4,9,16,32
		int networksize = atoi(args[2]);
		int flow = atoi(args[3]);//Amount of cars being spawned/ time step
		int ttrain = atoi(args[4]);//how many trainings until next group
		AVG=atoi(args[5]);	//set location of hotspot in middle
		STD= atoi(args[6]);//set std of distribution

		//Create initial partitioning
		std::vector<std::vector<int>> groups = inertialPartitioning( networksize, 1,  1 );
		printGroup(groups);
		groupToNetwork(groups, networksize);
		trainSpecific(groups, networksize,flow, false, ttrain);

		//calcualte groups to merge
		std::vector<int> ids = expandGroup(MOST_BLOCKED, networksize);
		groups = mergeGroups(groups, ids);
		printGroup(groups);
		groupToNetwork(groups, networksize);
		trainSpecific(groups,networksize, flow, false, ttrain);

		//Train overlaping groups.
		//
	}


	// ./Main specific $FLOW$
	if(std::string(args[1]) == "specific"){
		if (argc < 4){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		//set location of hotspot in middle


		int flow = atoi(args[2]);
		int ttrain = atoi(args[3]);

		AVG=atoi(args[4]);
		//set std of distribution
		STD= atoi(args[5]);

		//Trains & Creates Q greedy policies for each group
		string temp = "results/specific_"+to_string(flow);
		FILENAME = temp.c_str();
		std::vector<std::vector<int>> groups;


		// avg =1 std = 2, flow= 6 , ttrain = 5, network = 36
		groups = {{0,3,8,9 }, {2,1,10,31 }, {4,5,12,13 }, {6,7,14,15 }, {40,60,17,25 }, {45,19,26,27 }, {20,21,28,29 }, {22,57,30,32 }, {11,33,16,41 }, {34,35,42,43 }, {36,37,44,18 }, {38,39,46,47 }, {48,49,56,51 }, {50,23,58,59 }, {52,53,24,61 }, {54,55,62,63 } };
		int networksize = 1;
		//groups = {{3},{7},{11},{12},{13},{14},{15}, {0,1,4,5},{2},{6},{8},{9},{10}};
		trainSpecific(groups,networksize, flow, false, ttrain);
	}

	//start with simple partitioning, improve with 2exchange
	if(std::string(args[1]) == "exchange"){
		// size 1,4,9,16,32
		TEST = false;
		int groupsize = atoi(args[2]);
		// should always be to power of 2
		int networksize = atoi(args[3]);

		//set location of hotspot in middle

		int flow = atoi(args[4]);
		int ttrain = atoi(args[5]);

		AVG=atoi(args[6]);
		//set std of distribution
		STD= atoi(args[7]);
		string temp = "results/exchange_" + to_string(flow) + ".txt";
		FILENAME = temp.c_str();
		trainExchange(groupsize,groupsize, networksize, 1000 , flow, ttrain,3);
	}


	//calcualte community
	if(std::string(args[1]) == "community"){
		if (argc < 6){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		//get parameters
		int networksize = atoi(args[2]);
		int flow = atoi(args[3]);
		int ttrain = atoi(args[4]);

		AVG= atoi(args[5]);
		//set std of distribution7
		STD= atoi(args[6]);

		//get data from simulation
		Simulation sim(networksize, 0, false);
		std::vector<int> data = sim.getHeatmap(3000);

		//size of community
		int size = 1;
		//Calcuate communities from data
		std::vector<std::vector<int>> groups = createCommunities(data, size);
		groupToNetwork(groups, networksize);
		groups = expand_surounding(groups , networksize);
		groupToNetwork(groups, networksize);
		//Trains & Creates Q greedy policies for each group
		string temp = "results/community_"+to_string(flow);
		FILENAME = temp.c_str();
		//Train groups
		trainSpecific(groups,networksize, flow, false, ttrain);

	}


	//first calculate community then improve with 2exchange
	if(std::string(args[1]) == "community_exchange"){
		if (argc < 6){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		TEST = false;
		//get parameters
		int networksize = atoi(args[2]);
		int flow = atoi(args[3]);
		int ttrain = atoi(args[4]);

		AVG= atoi(args[5]);
		//set std of distribution7
		STD= atoi(args[6]);
		//get data from simulation
		Simulation sim(networksize, 0, false);
		std::vector<int> data = sim.getHeatmap(3000);
		//Calcuate communities from data
		int size = 1;

		std::cout << "Create initial solution" <<std::endl;
		std::vector<std::vector<int>> groups = createCommunities(data, size);
		groups = expand_surounding(groups , networksize);
		//Trains & Creates Q greedy policies for each group
		string temp = "results/community_"+to_string(flow);
		FILENAME = temp.c_str();
		//Train groups
		std::cout << "Start 2-exchange"<<std::endl;
		trainExchange(groups,networksize, 100 , flow, ttrain,3);

	}




	//prints heatmap given parameters
	if(std::string(args[1]) == "heatmap"){
		if (argc < 4){
			std::cout<< "Not enough parameters passed" << std::endl;
			return 0;
		}
		//get parameters
		int networksize = atoi(args[2]);
		int avg = atoi(args[3]);
		int std = atoi(args[4]);
		int div = atoi(args[5]);

		//get data from simulation
		Simulation sim(networksize, 0, false);
		sim.avg = avg;
		sim.std = std;

		std::vector<int> data = sim.getHeatmap(3000);

		for (int i =0; i < sqrt(networksize) ; i++){
			for (int j =0; j < sqrt(networksize) ; j++){
					std::cout << (double)data[j+ i*sqrt(networksize)]/div << " ";
			}
			std::cout  <<std::endl;
		}

		int size = 1;

		std::vector<std::vector<int>> groups = createCommunities(data, size);

		expand_surounding(groups, networksize);

	}



  return 0;
}
