namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;
#include <string>
using namespace std;
#include <cmath>
#include <fstream>
#include <math.h>
#include <iostream>

//Returns local density
std::vector<int> localDensity(std::vector<int> g){
	std::vector<int> res;
	int h= sqrt( g.size());
	for (int i = 0; i < h; i++){
		for (int j = 0; j < h; j++){
			int s =0;
			if( j > 0){
				s+= g[(i*h)+(j-1)];
			}
			if( j < h-1){
				s+= g[(i*h)+(j+1)];
			}
			if( i > 0){
				s+= g[((i-1)*h) +j];
			}
			if( i < h-1){
				s+= g[((i+1)*h)+j];
			}
			res.push_back(s);
		}
	}
	return res;
}


//Find how far from closest larger neighbor
int findLarger(std::vector<int>g, int x, int y){
	int i =1;
	int h= sqrt( g.size());
	while (i<30){
		for( int j = -i; j <= i;j++){
			if ((x+j >=0) && (x+j < h) && (y+i < h) && (g[(x*h)+y] < g[((x+j)*h)+y+i])){
                		return i;
			}
            		if ((x+j < h) && (x+j >=0) && (y-i >= 0)  && (g[(x*h)+y] < g[((x+j)*h)+y-i])){
               			return i;
            		}
			if ((y+j < h) && (y+j >= 0) && (x+i < h) && (g[(x*h)+y] < g[((x+i)*h)+y+j])){
               			return i;
            		}
			if ((y+j < h) && (y+j >= 0) && (x-i >= 0) && (g[(x*h)+y] < g[((x-i)*h)+y+j])){
                		return i;
			}
		}
		i+=1;
	}
	return -1;
}

std::vector<int> relativeDistance(std::vector<int> g){
	std::vector<int> res;
	int h = sqrt(g.size());
	for (int i = 0; i< h ; i++ ){
		for (int j = 0; j< h; j++ ){
			res.push_back(findLarger(g,i,j));
		}
	}
	int maximum = *max_element(res.begin(), res.end())+1;

	std::replace(res.begin(), res.end(), -1, maximum );
	return res;
}


//Find centers
std::list<int> findCenters(std::vector<int> g){
	int h = sqrt(g.size());
	std::vector<int> temp2(g.size());
	std::copy ( g.begin(), g.end(), temp2.begin());
	sort(temp2.begin(), temp2.end(), greater<int>());
	std::vector<int> temp(1);
	std::copy ( temp2.begin(), temp2.begin()+1, temp.begin());

	std::vector<int>::iterator it = std::find(g.begin(), g.end(), temp[0]);
	int index = std::distance(g.begin(), it);
	std::list<int> centers;
	centers.push_back(index);
	g[index] = -1;
	temp.erase(temp.begin());
	while(temp.size()>0){
		std::vector<int>::iterator it = std::find(g.begin(), g.end(), temp[0]);
		int index = std::distance(g.begin(), it);
		bool add = true;
		for (int j = 0; j< centers.size(); j++){
			int value = *(std::next(centers.begin(), j));
			int x1 = (int)value / h;
			int y1 = value  % h;
			int x2 = (int)index / h;
			int y2 = index  % h;
			if(sqrt( pow(x1-x2, 2) + pow(y1-y2, 2) ) < 2 ){
				add =false;
			}
		}
		if(add){
			centers.push_back(index);
			g[index] = -1;
		}
		temp.erase(temp.begin());
	}
	return centers;
}

//Expand group
std::list<int> expandCommunity(std::list<int> *u, std::list<int> c, int h ){
	std::list <int> res ;
	for (int i=0; i< c.size(); i++){
		int index = *(std::next(c.begin(), i));
		if ((index+1)%h != 0 && std::count (u->begin(), u->end(), index+1) >0 ){
			res.push_back(index+1);
			u->remove(index+1);
		}
		if ((index)%h != 0 && std::count (u->begin(), u->end(),index-1) >0 ){
			res.push_back(index-1);
			u->remove(index-1);
		}
		if (index < h*(h-1) && std::count (u->begin(), u->end(),index+h) >0 ){
			res.push_back(index+h);
			u->remove(index+h);
		}
		if (index > h && std::count (u->begin(), u->end(),index-h) >0 ){
			res.push_back(index-h);
			u->remove(index-h);
		}
		if (index+1 < h*(h-1) && (index+1)%h != 0 && std::count (u->begin(), u->end(),index+1+h) >0 ){
			res.push_back(index+h+1);
			u->remove(index+h+1);
		}
		if (index > h && (index)%h != 0 && std::count (u->begin(), u->end(),index-1-h) >0 ){
			res.push_back(index-h-1);
			u->remove(index-h-1);
		}
		if (index+1 < h*(h-1) && (index)%h != 0 && std::count (u->begin(), u->end(),index+h-1) >0 ){
			res.push_back(index+h-1);
			u->remove(index+h-1);
		}
		if (index > h && (index+1)%h != 0 && std::count (u->begin(), u->end(),index+1-h) >0 ){
			res.push_back(index-h+1);
			u->remove(index-h+1);
		}
	}
	return res;
}


//Expand center to form community
std::list<int> communityExpansion(std::vector<int> g,std::list<int> *u, int center, std::list<int> centers, int nr){
	std::list<int> C = {center};
	for (int x=0; x< nr;x++){
		std::list<int> Q = expandCommunity(u, C, sqrt(g.size()));
		C.merge(Q);
	}
	return C;
}


//create communities based on structural center.
std::vector<std::vector<int>> createCommunities(std::vector<int> g, int size){
		//calculate relative distance
		std::vector<int> RD = relativeDistance(g);
		//calculate local density
		std::vector<int> LD = localDensity(g);
		//structural centrality
		std::vector<int> SC;
		for (int i = 0 ; i < g.size();i++){
			SC.push_back(RD[i]*g[i]);
		}

		//find structural centers from SC
		std::list<int> centers = findCenters(SC);
		std::vector<int> res(g.size());

		int i = 1;

		std::list<int> u;
		for (int x=0; x< g.size();x++){
			u.push_back(x);
		}
		for (int o : centers){
			u.remove(o);
		}

		//expand centers
		for (std::list<int>::iterator it=centers.begin(); it!=centers.end(); ++it){
			std::list<int> community = communityExpansion(g,&u, *it, centers,size);
			for (std::list<int>::iterator it2=community.begin(); it2!=community.end(); ++it2){
				res[*it2] = i;
			}
			i++;
		}

		std::vector<std::vector<int>> groups = {};
		std::vector<int> comm = {};

		//convert network into groups
		for(int i = 0;i< sqrt(res.size());i++){
			for(int j = 0;j< sqrt(res.size());j++){
				//cout << res[i*sqrt(res.size())+j] << " ";
				if(res[i*sqrt(res.size())+j] == 1){
					comm.push_back(i*sqrt(res.size())+j);
				}else{
					std::vector<int> gr { (int)(i*sqrt(res.size())+j)};
					groups.push_back(gr);
				}
			}
			//cout <<endl;
		}
		groups.push_back(comm);
		return groups;
	}


	//Instead of starting from empty solution
	//Create inertial partitioning
	//merge groups surrounding the structural center
	std::vector<std::vector<int>> mergeCommunities(std::vector<int> g, int size){
			//calculate relative distance
			std::vector<int> RD = relativeDistance(g);
			//calculate local density
			std::vector<int> LD = localDensity(g);
			//structural centrality
			std::vector<int> SC;
			for (int i = 0 ; i < g.size();i++){
				SC.push_back(RD[i]*g[i]);
			}

			//find structural centers from SC
			std::list<int> centers = findCenters(SC);
			// create intertial partitioning
			std::vector<std::vector<int>> init = inertialPartitioning(g.size(), 2, 2);

			//nr of groups on 1 line.
			int nr = sqrt(g.size())/2;

			// find group id that contains center
			for(int i = 0; i<init.size(); i++){
					//if(std::find(init[i].begin(), init[i].end(), centers[0]) != init[i].end()){
							//merge groups
							//i
							//i+1

					//}
			}
			return init;
	}

	std::vector<std::vector<int>> expand_surounding(std::vector<std::vector<int>> groups, int network_size){
				std::vector<int> comm = groups.back();

				int ymin = ((int)comm[0] / sqrt(network_size)) -3;
				int xmin = (int)comm[0] % (int)sqrt(network_size) -2;
				int ymax = (int)comm.back()  / sqrt(network_size)+1;
				int xmax = (int)comm.back()  % (int)sqrt(network_size) +1;

				//std::cout << ymin <<", "<< xmin<<", " << ymax <<","<< xmax<<std::endl;
				// above
				// above
				if (ymin >= 0){
				  for (int xcurrent = xmin; xcurrent < xmax ; xcurrent +=1){
				    if(xcurrent >=0 && xcurrent <sqrt(network_size)-1){
				        //top left id of new group
				        int temp = ymin*sqrt(network_size)+ xcurrent;
				        std::vector<int> lis { temp };
				        groups.erase(std::remove(groups.begin(), groups.end(), lis), groups.end());
				        std::vector<int> lis2 { temp +1};
				        groups.erase(std::remove(groups.begin(), groups.end(), lis2 ), groups.end());
				        std::vector<int> lis3 { temp+(int)sqrt(network_size) };
				        groups.erase(std::remove(groups.begin(), groups.end(), lis3), groups.end());
				        std::vector<int> lis4 { temp+(int)sqrt(network_size)+1 };
				        groups.erase(std::remove(groups.begin(), groups.end(), lis4), groups.end());
				        //add new group
				        groups.push_back({ temp, temp+1, temp+(int)sqrt(network_size), temp+ (int)sqrt(network_size) +1 });
				        xcurrent += 1;
				    }
				  }
				  ymin+=2;
				}
				//left
				if (xmin >= 0){
				  for (int xcurrent = ymin; xcurrent <= ymax ; xcurrent +=1){
				    if(xcurrent >=0 && xcurrent <sqrt(network_size)-1){
				      int temp = xcurrent*sqrt(network_size)+ xmin ;
				      std::vector<int> lis { temp };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis), groups.end());
				      std::vector<int> lis2 { temp +1};
				      groups.erase(std::remove(groups.begin(), groups.end(), lis2 ), groups.end());
				      std::vector<int> lis3 { temp+(int)sqrt(network_size) };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis3), groups.end());
				      std::vector<int> lis4 { temp+(int)sqrt(network_size)+1 };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis4), groups.end());
				      //add new group
				      groups.push_back({ temp, temp+1, temp+(int)sqrt(network_size), temp+ (int)sqrt(network_size) +1 });
				        xcurrent += 1;
				    }
				  }
				  xmin +=2;
				}

				//right
				if (xmax < sqrt(network_size)-1 ){
				  for (int xcurrent = ymin; xcurrent <= ymax ; xcurrent +=1){
				    if(xcurrent >=0 && xcurrent <sqrt(network_size)-1){
				      int temp = xcurrent*sqrt(network_size)+ xmax ;
				      std::vector<int> lis { temp };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis), groups.end());
				      std::vector<int> lis2 { temp +1};
				      groups.erase(std::remove(groups.begin(), groups.end(), lis2 ), groups.end());
				      std::vector<int> lis3 { temp+(int)sqrt(network_size) };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis3), groups.end());
				      std::vector<int> lis4 { temp+(int)sqrt(network_size)+1 };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis4), groups.end());
				      //add new group
				      groups.push_back({ temp, temp+1, temp+(int)sqrt(network_size), temp+ (int)sqrt(network_size) +1 });
				        xcurrent += 1;
				    }
				  }
				  xmax -= 1;
				}



				//under
				if (ymax < sqrt(network_size)-1 ){
				  for (int xcurrent = xmin; xcurrent < xmax ; xcurrent +=1){
				    if(xcurrent >=0 && xcurrent <sqrt(network_size)-1){
				      int temp = ymax*sqrt(network_size)+ xcurrent;
				      std::vector<int> lis { temp };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis), groups.end());
				      std::vector<int> lis2 { temp +1};
				      groups.erase(std::remove(groups.begin(), groups.end(), lis2 ), groups.end());
				      std::vector<int> lis3 { temp+(int)sqrt(network_size) };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis3), groups.end());
				      std::vector<int> lis4 { temp+(int)sqrt(network_size)+1 };
				      groups.erase(std::remove(groups.begin(), groups.end(), lis4), groups.end());
				      //add new group
				      groups.push_back({ temp, temp+1, temp+(int)sqrt(network_size), temp+ (int)sqrt(network_size) +1 });
				        xcurrent += 1;
				    }
				  }
				}

				return groups;

	}
