namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;
#include <string>
using namespace std;

#include <fstream>
#include <math.h>
#include <iostream>


int TESTS = 30;		//Amount of tests run during test
int EPISODE = 1000;	//Amount of steps in 1 episode
int TRAINING = 10000;	//Amount of Training periods
int AVG = 1;
int STD = 1;

int MOST_BLOCKED = 0;

bool TEST = true;
const char* FILENAME;

//Prints groups as lists
void printGroup(std::vector<std::vector<int>> groups ){
	for (int i = 0; i < groups.size() ; i++){
		std::cout << "{";
		for (int j = 0; j < groups[i].size(); j++){
			std::cout << groups[i][j] << ", ";
		}
		std::cout << "}, ";
	}
	std::cout << std::endl;
}

//Expand group
//id: id of group that should be expanded
std::vector<int> expandGroup(int id , int networksize){
	int x = id % (int)sqrt(networksize);
	int y = (int)id / sqrt(networksize);
	int xmin = (x==0) ? 0 : -1;
	int xmax = (x == sqrt(networksize)-1 ) ? 1 : 2;
	int ymin = (y==0) ? 0 : -1;
	int ymax = (y == sqrt(networksize)-1 ) ? 1 : 2;

	std::vector<int> ids;
	for (int i = ymin ; i < ymax; i++){
		for (int j = xmin ; j < xmax; j++){
				ids.push_back(id+j+(i*sqrt(networksize)));
		}
	}
	return ids;
}


//merge given set of groups into 1
//groups: list of all groups.
//ids: list of groups that need to be merged.
std::vector<std::vector<int>> mergeGroups(std::vector<std::vector<int>> groups, std::vector<int> ids){
	//super group will replace merged groups.
	std::vector<int> supergroup;
	//create supergroups
	for(int i = 0 ; i < ids.size(); i++){
		supergroup.insert(supergroup.end(), groups[ids[i]].begin(), groups[ids[i]].end());
	}
	//remove groups
	for(int i = ids.size()-1 ; i>=0 ; i--){
		groups.erase(groups.begin()+ids[i]);
	}
	//sort supergroup and add to rest
	sort(supergroup.begin(), supergroup.end());
	groups.push_back(supergroup);
	return groups;
}


// create groups with intertial partitioning
std::vector<std::vector<int>> inertialPartitioning(int networksize, int h, int v){
	int x = sqrt(networksize);
	int groupsize = h*v;
	//create initial partitiioning
	std::vector<std::vector<int>> current;
  //r = 0 -> nr_groups
	for( int r = 0; r < networksize/groupsize; r++){
		int temp = (r%(x/h))*h;// move ids to right(add h)
		temp += (r/(x/v))*(v*x); // move ids down (add v)
    //temp = id of topleft agent in group r
		std::vector<int> ids;
		for( int i = 0; i < v; i++){
			for( int j = 0; j < h; j++){
        //calculating ids in group r
				int id = j+i*x+temp;
				ids.push_back(id);
			}
		}
		current.push_back(ids);
	}
	return current;
}

// create overlapping groups with agents part of multiple groups
// overlap: amount of agents overlap with 2 neighboring groups.
std::vector<std::vector<int>> inertialPartitioning(int networksize, int h, int v, int overlap){
	int m = sqrt(networksize);
	int groupsize = h*v;
	std::vector<std::vector<int>> current;
	int nr_groups = networksize/((h-overlap)*(v-overlap));
	for( int x = 0; x < sqrt(nr_groups) ; x++){
		for( int y = 0; y < sqrt(nr_groups) ; y++){
			if(x*(v-overlap)+h <= m && y*(h-overlap)+v <= m ){
				int temp = (m*x*(v-overlap))+(y*(h-overlap));
				std::vector<int> ids;
				for( int i = 0; i < v; i++){
					for( int j = 0; j < h; j++){
						//calculating ids in group r
						int id = j+i*m+temp;
						ids.push_back(id);
					}
				}
				current.push_back(ids);
			}
		}
	}
	return current;
}

//find multiples
//returns lists of locations in groups with same agent id
std::vector<std::vector<std::vector<int>>> findMultiples(std::vector<std::vector<int>> groups, int networksize){
	std::vector<std::vector<std::vector<int>>> multiples;
	//search for every id
	for (int id = 0 ; id < networksize; id++){
		std::vector<std::vector<int>> list;
		//which group
		for (int i = 0 ; i < groups.size();i++){
			//location in group
			for (int j = 0 ; j < groups[i].size();j++){
				//if id occurs, store in list
				if( groups[i][j] == id ){
					list.push_back({i, j});
				}
			}
		}
		//if it occurs in more than group it should be saved
		if(list.size() > 1){
			multiples.push_back(list);
		}
	}
	return multiples;
}



// create overlapping groups, agents not part of multiple groups
std::vector<std::vector<int>> overlapPartitioning(int networksize, int h, int v){
  std::vector<std::vector<int>> current;
	int x = sqrt(networksize);
	int groupsize = h*v;
	//create initial partitiioning
  int nr_groups = networksize/groupsize;

  for( int k = 0; k<sqrt(nr_groups); k++){
    for( int l = 0; l<sqrt(nr_groups); l++){
        //id of top left
        int temp = k*sqrt(networksize)+l;
        std::vector<int> ids;
        //create groups
        for( int i = 0; i < v; i++){
    			for( int j = 0; j < h; j++){
            int step = sqrt(networksize)/h;
    				int id = temp+(j*step)+(i*step*x);
    				ids.push_back(id);
    			}
    		}
    		current.push_back(ids);
    }
  }


	return current;
}

// create groups with overlappingborders
std::vector<std::vector<int>> overlapborderPartitioning(int networksize, int h, int v){
  //create initial partitioing.
  std::vector<std::vector<int>> current = inertialPartitioning(networksize, h, v);
  //swap edge agents with neighboring groups.
	int x = sqrt(networksize);
	int groupsize = h*v;
	//create initial partitiioning
  int nr_groups = networksize/groupsize;
  int y = (int) sqrt(nr_groups);
  //always swap corners, only swap sides if bigger than 2
  for (int i=0 ; i< y; i++){
    for (int k=0 ; k< y; k++){
      int group = k + i*y;
      //swap with group under
      if(i < y-1 ){
        for (int j = 1 ; j < h-1 ; j++){
            int temp = current[group][j+(h*(v-1))];
            current[group][j+(h*(v-1))] = current[group+y][j];
            current[group+y][j] = temp;

        }
      }
      //swap with group right
      if(k < y-1){
        for (int j = 1 ; j < v-1 ; j++){
            int temp = current[group][(j*h)+(h-1)];
            current[group][(j*h)+(h-1)] = current[group+1][j*h];
            current[group+1][j*h] = temp;
        }
      }


    }
  }
	return current;
}


void groupToNetwork(std::vector<std::vector<int>> groups, int networksize){
  int i = 0;
  std::vector<int> net;
   while (i < networksize){
     for (int j = 0; j < groups.size(); j++) {
       for(int l = 0; l < groups[j].size(); l++ ){
          if(groups[j][l] == i){
            net.push_back(j);
          }
	     }
	   }
	   i++;
   }
   for (int j = 0; j < sqrt(networksize); j++) {
      for(int l = 0; l < sqrt(networksize); l++ ){
        std::cout << net[j*sqrt(networksize)+l]<< " ";
      }
         std::cout <<std::endl;
   }
}


void printSolution(std::vector<int> vector){
   	printf("The solution:\n");
	std::cout <<"[ ";
	for (auto & i : vector) {
    		std::cout << i <<" ";
	}
	std::cout <<"]\n";
}

void printVector(std::vector<long unsigned int> vector){
   	printf("The solution:\n");
	std::cout <<"[ ";
	for (auto & i : vector) {
    		std::cout << i <<" ";
	}
	std::cout <<"]\n";
}



// get sub vector
vector<int> sub(vector<int> v, int m, int n) {
	auto first = v.begin() + m;
	auto last = v.begin() + n + 1;
	vector<int> vector(first, last);
	return vector;
}


// saves solution to text file
void save_solution(std::vector<int> result){
	std::ofstream outputFile("solution.txt");
	std::copy(result.rbegin(), result.rend(), std::ostream_iterator<int>(outputFile, "\n"));
	outputFile.close();
}

void createFile(string  result){
	system("mkdir -p results");
	std::ofstream outputFile;
	outputFile.imbue(std::locale(""));
	outputFile.open(FILENAME, std::ios_base::app); // append instead of overwrite
	outputFile << result << std::endl;
	outputFile.close();
}

void append_test(float result){
	std::ofstream outputFile;
	outputFile.imbue(std::locale(""));
	outputFile.open(FILENAME, std::ios_base::app); // append instead of overwrite
	outputFile << result << std::endl;
	outputFile.close();
}

void append_done(){
	std::ofstream outputFile;
	outputFile.imbue(std::locale(""));
	outputFile.open(FILENAME, std::ios_base::app); // append instead of overwrite
	outputFile << "PreTrainingDone" << std::endl;
	outputFile.close();
}


//loads solution from text file
std::vector<int> load_solution(){
	ifstream inputFile("solution.txt");
	std::vector<int> res;
	std::istream_iterator<int> input(inputFile);
	std::copy(input, std::istream_iterator<int>(), std::back_inserter(res));
	inputFile.close();
	std::reverse(res.begin(),res.end());
	return res;
}
