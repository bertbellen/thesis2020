namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;

using namespace std;
#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <algorithm>
#include <math.h>       /* sqrt */





//Test QGreedyPolicy
//Takes: previously trained Q greedyPolicies
//Runs simulation few times
float testNetwork(std::vector<fm::QGreedyPolicy> policies, std::vector<std::vector<int>> groups ,int networksize, int flow){
	//Create Simulation
	Simulation sim(networksize, flow, false);
	sim.spawnCar(networksize);


	//generate list of locations with same agent id.
	std::vector<std::vector<std::vector<int>>> multiples = findMultiples(groups, networksize);

	//Amount of groups
	int nr_groups = groups.size();
	std::vector<int> blocks(nr_groups, 0);
	float avg = 0;	//average reward
	float spwn = 0;	// average amount of cars spawn
	float avg_steps = 0;
	float avg_waiting = 0;
	int u = 0;
	double sampletime = 0;
	for(int i = 0; i< TESTS; i++){
		int steps = 0;
		while(steps < EPISODE && !sim.Blocked()){// Every 150 timesteps or until blocked
			steps += 1;
			std::vector<aif::Action> a0;
			// Sample action for each group, from its own greedy policy

			for( int j = 0; j < nr_groups; j++){
				clock_t start = clock();
				a0.push_back(policies[j].sampleAction(sim.getStates(groups[j])));
				clock_t end = clock();
				sampletime = (sampletime*u + difftime(end,start))/(u+1);
				u++;
			}


			//Find joint action for agents appearing in multiple groups.
			//perform action proposed by majority, else do random.
			for(int o = 0; o< multiples.size(); o++){
				//taking average of actions
				float avg = 0;
				for (int p = 0; p < multiples[o].size();p++){
					avg += a0[multiples[o][p][0]][multiples[o][p][1]];
				}
				avg /= multiples[o].size();
				int ac;
				// perform action 1
				if(avg > 0.5 ){
					ac = 1;
				}
				// perform action 0
				if(avg < 0.5 ){
					ac = 0;
				}
				// perform action random
				if(avg == 0.5 ){
					ac = rand()%2;
				}

				//Apply action to all those agents
				for (int p = 0; p < multiples[o].size();p++){
					a0[multiples[o][p][0]][multiples[o][p][1]] = ac;
				}
			}


			// Combine actions of groups into one action
			aif::Action action = aif::Action(networksize);
			for( int j = 0; j < nr_groups; j++){
				for( int l = 0; l < groups[j].size(); l++){
					action[groups[j][l]] = a0[j][l];
				}
			}

			//Apply action on simulation
			sim.applyActions(action);
			int k = 0;
			while(k<3){// run certain time until new action
				sim.step();
				k++;
			}

		}
		avg_steps += steps;
		avg += sim.reward;
		spwn += sim.spawned;
		avg_waiting += sim.waiting_time;
		if(i == 0 && TEST){//display state of first action
			sim.displayState();
		}

		for( int l = 0; l < nr_groups; l++){
			if(sim.groupBlocked(groups[l])){
				blocks[l]++;
			}
		}
		sim.reset();
	}

	//std::cout << "Avg sample time : "<<  sampletime  <<std::endl;

	MOST_BLOCKED = std::max_element(blocks.begin(),blocks.end()) - blocks.begin();

	if(TEST){
		std::cout << "Time until jammed: "<<avg_steps/TESTS << ", Most blocked group: "<<MOST_BLOCKED <<  std::endl ;
	}
	return avg_steps/TESTS;
}




// Let the network act totally random
void testRandom(int networkSize, int flow){
	//Create Simulation
	Simulation sim(networkSize, flow, false);
	int i=0;
	float avg = 0;	//average reward
	float spwn = 0;	// average amount of cars spawn
	sim.spawnCar(networkSize);	//Spawn cars
	int avg_steps = 0;
	for(int i = 0; i< TESTS; i++){
		int steps = 0;
		while(steps < EPISODE && !sim.Blocked()){// Until certain timesteps or until blocked
			steps += 1;
			// Random actions of groups into one action
			aif::Action action = aif::Action(networkSize);
			for( int j = 0; j < networkSize; j++){
				action[j] = rand()%2;
			}
			//Apply action on simulation
			sim.applyActions(action);
			int k = 0;
			while(k<3){// run certain time until new action
				sim.step();
				k++;
			}
		}
		avg_steps += steps;
		avg += sim.reward;
		spwn += sim.spawned;
		sim.reset();
	}
	std::cout << "Reached dest: "<< avg/spwn*100 << "%, Flow: " << spwn/avg_steps << " cars/step, Time until jammed: "<<avg_steps/TESTS << "\n";
}
