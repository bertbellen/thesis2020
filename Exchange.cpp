namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;
#include <string>
using namespace std;
#include <cmath>
#include <fstream>
#include <math.h>
#include <iostream>






//Perform 2exchange
//nr means amount of exchanges
std::vector<std::vector<int>> opt_exchange(std::vector<std::vector<int>> groups , int nr){
		int i = 0;
		while (i<nr){
			//groups to modify
			int group1	= rand() % groups.size();
			int group2	= rand() % groups.size();
			while(groups[group2].size() == 1){
				group2	= rand() % groups.size();
			}

			//agents to change from 1 group to the other
			int node1 = rand() % groups[group1].size();
			int node2 = rand() % groups[group2].size();
			//prevent swaping agent with itself
			while(group1==group2 && node1 == node2){
				node2 = rand() % groups[group2].size();
			}

			//perform exchange
			int temp = groups[group1][node1];
			groups[group1][node1] = groups[group2][node2];;
			groups[group2][node2] = temp;
			i++;
		}
		//return result
		return groups;
	}

	//train exchange on given configuration
	float trainExchange(std::vector<std::vector<int>> current,int networksize, int itterations, int flow, int ttrain, int nr){
		std::vector<std::vector<int>> best = current;
		float bestscore = 0;
		srand (time(NULL));
			int i = 0;
			while (i < itterations){
				std::vector<std::vector<int>> temp = opt_exchange(current,nr);
				std::cout << "Testing partitioning: " << std::endl;
				printGroup(temp);
				//test new partitioning
				float newscore = trainSpecific(temp, networksize,flow,false , ttrain);
				//with probablilty of 0.2 take worsening step
				//if( (rand() % 10) << 2){
				//		current = temp;
				//}

				//if new partitiong is better save it
				if( bestscore<newscore){
						current = temp;
						best = current;
						bestscore = newscore;
				}
				std::cout << "best score=" << bestscore << "; new score=" << newscore <<std::endl;
				i++;
			}
			return 0;
	}

//perform itterative improving with 2exchange on simple partitioning
float trainExchange(int h, int v, int networksize, int itterations, int flow, int ttrain, int nr){
	int x = sqrt(networksize);
	int groupsize = h*v;
	//create initial partitiioning
	std::vector<std::vector<int>> current;
	for( int r = 0; r < networksize/groupsize; r++){
		int temp = (r%(x/h))*h;// move ids to right(add h)
		temp += (r/(x/v))*(v*x); // move ids down (add v)
		std::vector<int> ids;
		for( int i = 0; i < v; i++){
			for( int j = 0; j < h; j++){
				int id = j+i*x+temp;
				ids.push_back(id);
			}
		}
		current.push_back(ids);
	}
	return trainExchange(current, networksize, itterations, flow,  ttrain,  nr );
}
