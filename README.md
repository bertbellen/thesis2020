**Main.cpp**
<!-- blank line -->
    Allows user to train different kind of partitionings.
    User input : Type of partitioning, size of network , size of groups, traffic flow, etc.


**SCQL.cpp**
<!-- blank line -->
    Creation of Sparse Cooperative Q-Learning
    Every agent has 2 state variabels:  1) Horizontal cars waiting inline,
                                        2) Vertical cars waiting inline

    Each variable can take 4 values:    1) 0% full,
                                        2) <25% full,
                                        3) <50%full,
                                        4) <100%full

    Every agent has 1 action which can take 2 values:   1) Horizontal green,
                                                        2) Vertical green

    The action of each agent depends on the action of each of its neighbors.
	Rules:
		H State of agent depends on V and H state agent right
		V State of agent right depends on V state of agent left

		V State of agent depends on H and V state agent under
		V State of agent under depends on H state of agent


    Reward of each agent is defind by:
            R = #Cars passed through - #cars waiting in line that is not green


**Simulation.cpp**
<!-- blank line -->
    Simulates a traffic light network (grid)


**Train.cpp**
<!-- blank line -->
    Will solve a given network configuration with sparse cooperative Q-learning.
    Using Epsilon-greedy update.

**Test.cpp**
<!-- blank line -->
    Will test the quality of a given sparse cooperative Q-learning policy.
    Using Epsilon-greedy.

**Community.cpp**
<!-- blank line -->
    Calculates communities in the network based on provided data ( https://doi.org/10.1371/journal.pone.0169355 )
    Groups are created based on these communities.


**Exchange.cpp**
<!-- blank line -->
    Implements iterative search using k-opt exchange heuristic.

**Utils.cpp**
<!-- blank line -->
    Utils contains helper functions (pretty print functions, saving data to files, etc ..) and functions to create different kinds of partitionings (inertial partitioning, overlapping ...)
