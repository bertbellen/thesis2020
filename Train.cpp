namespace aif = AIToolbox::Factored;
namespace fm = AIToolbox::Factored::MDP;
#include <pthread.h>
using namespace std;
#include <AIToolbox/Utils/Core.hpp>
#include <AIToolbox/Factored/Utils/Core.hpp>
#include <AIToolbox/Factored/Types.hpp>
#include <AIToolbox/Factored/MDP/Policies/QGreedyPolicy.hpp>
#include <AIToolbox/Factored/MDP/Utils.hpp>
#include <AIToolbox/Factored/MDP/CooperativeModel.hpp>
#include <algorithm>
#include <math.h>       /* sqrt */
#include "Test.cpp"
#include <ctime>
//Train SparseCooperativeQlearning
//Takes: agent network, groupsize, amount of timesteps training
//Return QgreedyPolicy

struct Scql createGroup(int h, int v){
	int x = h;
	int networksize = h*v;
	int groupsize = h*v;
	std::vector<struct Scql> groups;
	std::vector<int> ids;
	for( int i = 0; i < v; i++){
		for( int j = 0; j < h; j++){
			int id = j+i*x;
			ids.push_back(id);
		}
	}
	Scql group = {h,v,ids};
	return group;
}

struct thread_data {
	fm::SparseCooperativeQLearning* learner;
	struct Scql group;
	int flow;
	std::vector<int>* results;
};



float trainSpecific(std::vector<std::vector<int>> ids ,int networksize, int flow, bool pretrain, int ttrain){

	std::vector<struct Scql> groups;
	for (int i = 0; i< ids.size();i++){
		int h = sqrt(ids[i].size());
		Scql group = {h,h,ids[i]};
		groups.push_back(group);
	}

	//Create Simulation
	Simulation sim(networksize, flow,false);
	sim.spawnCar(networksize);

	//Learning rate
	double learning_rate= 0.0001;
	double epsilon = 3;
	//group that is currently trained.
	int active = 0;
	//Group to train
	int nr_groups = groups.size();
	std::vector<aif::Action> a0;


	//Create Q learners
	std::vector<fm::SparseCooperativeQLearning> learners;
	for( int i = 0; i < nr_groups; i++){
		learners.push_back(createSCQL(groups[i], learning_rate));
		a0.push_back(aif::Action (groups[i].Ids.size(), 0));
	}

	float best = 0;

	int episodes = 0;
	int trainings = 0;
	int counter = 0;
	double trainingtime =0;
	int n = 0;
	int itt = 5;

	//generate list of locations with same agent id.
	std::vector<std::vector<std::vector<int>>> multiples = findMultiples(ids, networksize);

	while(counter < itt){
		int steps = 0;
		while( (steps < EPISODE) && (sim.Blocked() == false) ){
			//state at t=0
			std::vector<aif::State> s0;
			// Get state variables of each group
			for( int j = 0; j < nr_groups; j++){
				s0.push_back(sim.getStates(groups[j].Ids));
			}


			// Combine actions of groups into one action
			aif::Action action = aif::Action(networksize);

			//decide if explore
			bool explore = rand()%10<epsilon;
			for( int j = 0; j < nr_groups; j++){
				for( int l = 0; l < groups[j].Ids.size(); l++){
					if(explore){
						a0[j][l] = rand()%2;
					}
					action[groups[j].Ids[l]] = a0[j][l];
				}
			}

			//Apply action on simulation
			sim.applyActions(action);

			int k = 0;
			while(k<3){// run certain timesteps until new action
				sim.step();
				k++;
			}

			// update Q of active group
			clock_t start = clock();
			a0[active] = learners[active].stepUpdateQ(s0[active], a0[active], sim.getStates(groups[active].Ids), sim.getReward(groups[active].Ids));
			clock_t end = clock();
			trainingtime = (trainingtime*n + difftime(end,start))/(n+1);
			n++;


			//get actions of other groups
			for( int j = 0; j < nr_groups ; j++){
				if(j == active) continue; //action of active group already done
				fm::QGreedyPolicy policy(learners[j].getS(),learners[j].getA(),learners[j].getQFunctionRules());

				if(ttrain==0){//update all agents at the same time
						a0[j] = learners[j].stepUpdateQ(s0[j], a0[j], sim.getStates(groups[j].Ids), sim.getReward(groups[j].Ids));
				}else{//sample action from agents
						a0[j] = policy.sampleAction(sim.getStates(groups[j].Ids));
				}
			}

			//Find joint action for agents appearing in multiple groups.
			//perform action proposed by majority, else do random.
			for(int o = 0; o< multiples.size(); o++){
				//taking average of actions
				float avg = 0;
				for (int p = 0; p < multiples[o].size();p++){
					avg += a0[multiples[o][p][0]][multiples[o][p][1]];
				}
				avg /= multiples[o].size();
				int ac;
				// perform action 1
				if(avg > 0.5 ){
					ac = 1;
				}
				// perform action 0
				if(avg < 0.5 ){
					ac = 0;
				}
				// perform action random
				if(avg == 0.5 ){
					ac = rand()%2;
				}

				//Apply action to all those agents
				for (int p = 0; p < multiples[o].size();p++){
					a0[multiples[o][p][0]][multiples[o][p][1]] = ac;
				}
			}


			trainings++;

			//test solution after 10 000 updates
			if((trainings+1)%10000 == 0 ){

				//std::fill (blocks.begin(),blocks.end(),0);
				//std::cout<< "avg training time: " << trainingtime << std::endl;
				counter++;
				std::cout << counter<<"/ "<< itt <<std::endl;
					std::vector<fm::QGreedyPolicy> policies;
					learning_rate = std::max(0.0001, learning_rate*=0.85);
					epsilon = std::max(1.0, epsilon*=0.95);
					for( int l = 0; l < groups.size(); l++){
						learners[l].setLearningRate(learning_rate);
						fm::QGreedyPolicy policy(learners[l].getS(),learners[l].getA(),learners[l].getQFunctionRules());
						policies.push_back(policy);
					}
					float res = testNetwork(policies,ids,networksize, flow);
					if(res >best){
						best = res;
					}
					append_test(res);
					episodes =0;
			}

			//Train other group after 'ttrain' updates
			//not necesar when training together
			if( (ttrain>0) && ((trainings+1)%(ttrain) == 0) ){
				//std::cout << "ttrain: " << ttrain*9/groups[active].Ids.size() <<std::endl;
				bool explore = rand()%10>4;
				if (true){//with certain probabilty pick group that caused system to block most of the times
					//active = std::max_element(blocks.begin(),blocks.end()) - blocks.begin();
					active = (active+1)%groups.size();
				}else{//with certain probabilty pick random group to train
					active =  rand()%groups.size();
				}

				//std::fill (blocks.begin(),blocks.end(),0);
				episodes += 1;
			}
			steps++;
		}
		sim.reset();
	}

	return best;
}
